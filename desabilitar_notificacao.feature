#language: pt
#encode: utf-8

Funcionalidade: Desabilitar notificações
    Eu como usuário do Whatsapp Web
    Desejo desabilitar as notificações de alerta de sons

    Cenário: Desabilitar notificações de sons
        Dado que eu esteja na tela de configurações
        E navegue até a tela de notificações
        Quando eu desabilitar opção de Sons
        Então as notificações foram desabilitadas

    Cenário: Desabilitar durante um tempo as notificações de sons
        Dado que eu esteja na tela de configurações
        E navegue até a tela de notificações
        Quando eu selecionar desativar sons e alertas 
        E selecionar 1 hora
        Então as notificações foram desabilitadas durante este tempo 