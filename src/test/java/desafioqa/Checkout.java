package desafioqa;

public class Checkout {

	private String itens;
	
	/**
	 * Constructor
	 */
	public Checkout(){
		this.itens = "";
	}	
	
	/**
	 * Method for scan and add new products
	 * @param products new products
	 */
	public void scan(String products){
		this.itens += products;
	}
	
	/**
	 * Method to calculate total value of products
	 * @return total value of products 
	 */
	public int total(){
		
		int qtdProductsA = 0
				, qtdProductsB = 0
				, valueTotal = 0;
		
		for (int i = 0; i < itens.length(); i++) {
				switch (itens.charAt(i)) {
				case 'A':
					valueTotal += 50;
					qtdProductsA += 1;
					if(qtdProductsA == 3){
						qtdProductsA = 0;
						valueTotal -= 20;
					}
					break;
				case 'B':
					valueTotal += 30;
					qtdProductsB += 1;
					if(qtdProductsB == 2){
						qtdProductsB = 0;
						valueTotal -= 15;
					}
					break;
				case 'C':
					valueTotal += 20;
					break;
				case 'D':
					valueTotal += 15;
					break;
				default:
					break;
				}
		}		
		return valueTotal;
	}
}