#language: pt
#encode: utf-8

Funcionalidade: Realizar alterações do perfil
    Eu como usuário do Whatsapp Web
    Desejo alterar ou excluir informações do meu perfil

    Cenário: Alterar meu nome no perfil
        Dado que eu esteja na tela de Perfil 
        E eu navegar até editar nome
        Quando eu alterar o nome do Perfil
        Então alteração é feita com sucesso

    Cenário: Remover minha foto de perfil
        Dado que eu esteja na tela de Perfil
        E selecionei alterar foto de perfil
        E selecionei remover foto
        Quando eu confirmar
        Então exclusão é feita com sucesso